import React, { Component } from "react";
import userStore from "./components/userStore/userStore";
import Login from "./components/Login/Login";
// import InputField from "./components/InputField/InputField";
import SubmitButton from "./components/SubmitButton/SubmitButton";
import { observer } from "mobx-react";
import axios from "axios";
import "./App.scss";

class App extends Component {
    async componentDidMount() {
        try {
            let res = await {
                method: "post",
                url: "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in",
                data: {
                    email: "testeapple@ioasys.com.br",
                    password: "12341234"
                }
            };

            axios(res);

            let result = await res.json();
            console.log(result);
            if (result && result.sucess) {
                userStore.loading = true;
                userStore.isLoggedIn = true;
                userStore.username = result.username;
            } else {
                userStore.loading = false;
                userStore.isLoggedIn = false;
            }
        } catch (e) {
            userStore.loading = false;
            userStore.isLoggedIn = false;
        }
    }

    async doLogout() {
        try {
            let res = await fetch("/logout", {
                method: "post",
                headers: {
                    Accept: "application/json",
                    "Content-type": "application/json"
                }
            });

            let result = await res.json();

            if (result && result.sucess) {
                userStore.isLoggedIn = false;
                userStore.username = "";
            }
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        if (userStore.loading) {
            return (
                <div>
                    <h1>LOGADO</h1>
                </div>
            );
        } else {
            if (userStore.isLoggedIn) {
                return (
                    <div>
                        <h1>
                            DESLOGADO {userStore.username}
                            <SubmitButton
                                text={"LogOut"}
                                disabled={false}
                                onClick={() => this.doLogout()}
                            />
                        </h1>
                    </div>
                );
            } else {
                return (
                    <div>
                        <Login />
                    </div>
                );
            }
        }
    }
}

export default observer(App);
