import { extendObservable } from "mobx";

/**
 * UserStore
 */

class userStore {
    constructor() {
        extendObservable(this, {
            loading: true,
            isLoggedIn: false,
            username: ""
        });
    }
}

export default new userStore();
