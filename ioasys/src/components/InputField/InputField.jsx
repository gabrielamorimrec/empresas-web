import React, { Component } from "react";
import "./InputField.scss";

export default class InputField extends Component {
    render() {
        return (
            <div>
                <div className="input-container">
                    <div className="input-container--svgAlign">
                        <img src={this.props.icone} alt="carta" />
                        <input
                            type={this.props.type}
                            placeholder={this.props.placeholder}
                            value={this.props.value}
                            onChange={(e) =>
                                this.props.onChange(e.target.value)
                            }
                        />
                    </div>
                    <i className="zmdi zmdi-account zmdi-hc-lg"></i>
                </div>
            </div>
        );
    }
}
