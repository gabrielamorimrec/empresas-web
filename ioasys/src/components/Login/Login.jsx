import React, { Component } from "react";
import InputField from "../InputField/InputField";
import SubmitButton from "../SubmitButton/SubmitButton";
import userStore from "../userStore/userStore";
import logo from "../../assets/ioasys_logo.png";
import carta from "../../assets/svg/ic-email.svg";
import cadeado from "../../assets/svg/ic-cadeado.svg";
import axios from "axios";
import "./Login.scss";

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            buttonDisable: true
        };
    }

    setInputValue(property, value) {
        value = value.trim();

        if (value.length > 30) {
            return;
        }
        this.setState({
            [property]: value
        });
    }

    resetForm() {
        this.setState({
            username: "",
            password: "",
            buttonDisable: false
        });
    }

    async doLogin() {
        if (!this.state.username) {
            return;
        }
        if (!this.state.password) {
            return;
        }

        this.setState({
            buttonDisable: false
        });

        try {
            let res = {
                method: "post",
                url: "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in",
                data: {
                    email: this.state.username,
                    password: this.state.password
                }
            };

            axios(res);

            let result = await res.json();
            if (result && result.success) {
                userStore.isLoggedIn = true;
                userStore.username = result.username;
            } else if (result && result.success === false) {
                this.resetForm();
                alert(result.msg);
            }
        } catch (e) {
            console.log(e);
            this.resetForm();
        }
    }

    render() {
        return (
            <div className="alignments">
                <div>
                    <img src={logo} alt="ioasyslogo" />
                    <h2>BEM VINDO AO EMPRESAS</h2>
                    <p>
                        Lorem ipsum dolor sit amet, contetur adipiscing elit.
                        Nunc accumsan.
                    </p>
                </div>
                <InputField
                    type="text"
                    placeholder="USERNAME"
                    icone={carta}
                    value={this.state.username ? this.state.username : ""}
                    onChange={(value) => this.setInputValue("username", value)}
                />
                <InputField
                    type="password"
                    placeholder="PASSWORD"
                    icone={cadeado}
                    value={this.state.password ? this.state.password : ""}
                    onChange={(value) => this.setInputValue("password", value)}
                />
                <SubmitButton
                    text="ENTRAR"
                    disabled={!this.state.buttonDisable}
                    onClick={() => this.doLogin()}
                />
            </div>
        );
    }
}
