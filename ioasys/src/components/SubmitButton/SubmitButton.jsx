import React, { Component } from "react";

export default class SubmitButton extends Component {
    render() {
        return (
            <div>
                <button
                    disabled={this.props.disabled}
                    onClick={() => this.props.onClick()}
                >
                    {this.props.text}
                </button>
            </div>
        );
    }
}
